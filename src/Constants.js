async function getCurrencyCodeToCurrencyNameList() {
    const currencyCodeToNameResponse = await fetch("https://openexchangerates.org/api/currencies.json");
        const currencyCodeToNameData = await currencyCodeToNameResponse.json();
        let currencyCodeToNameList = {}
        Object.keys(currencyCodeToNameData).forEach(currencyCode =>{
            currencyCodeToNameList[currencyCode] = currencyCodeToNameData[currencyCode]
        })
    return currencyCodeToNameList
}

async function getCurrencyNameToCurrencyCodeList() {
    const currencyCodeToNameResponse = await fetch("https://openexchangerates.org/api/currencies.json");
        const currencyCodeToNameData = await currencyCodeToNameResponse.json();
        let currencyNameToCodeList = {}
        Object.keys(currencyCodeToNameData).forEach(currencyCode =>{
            currencyNameToCodeList[currencyCodeToNameData[currencyCode]] = currencyCode
        })
    return currencyNameToCodeList
}

var currencyCodeToNameList = getCurrencyCodeToCurrencyNameList()
var currencyNameToCodeList = getCurrencyNameToCurrencyCodeList()
