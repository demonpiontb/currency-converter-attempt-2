import React from 'react'
import { useState } from 'react'
import './converter.css';
import * as Constants from'./Constants.js'

export default function CurrencyConverter() {

    const[currencyExchangeRateList, setCurrencyExchangeRateList] = useState({})                 //currency exchange list based on RUB, is used to do calculations
    const[currencyCodeToCurrencyNameList, setCurrencyCodeToCurrencyNameList] = useState({})     //two lists of dependencies |currency code -> currency name| & |currency name -> currency code|
    const[currencyNameToCurrencyCodeList, setCurrencyNameToCurrencyCodeList] = useState({})     //are used to show full currency name in dropbox
    const[resultsList, setResultList] = useState({})                                            //list of results to show in table
    var currencyAmountRef = React.createRef()
    var currentBaseRef = React.createRef()

    //This function called once to load actual data of exchange rates and |currency code -> currency name| dependencies.
    const createDataArrays = async () => {
        const exchangeRateResponse = await fetch("https://www.cbr-xml-daily.ru/latest.js");
        const exchangeRateData = await exchangeRateResponse.json();
        let currencyExchangeTempList = {}
        Object.keys(exchangeRateData.rates).forEach(currencyCode =>{
            currencyExchangeTempList[currencyCode.toString()] = exchangeRateData.rates[currencyCode]
        })
        currencyExchangeTempList["RUB"] = 1.0000
        setCurrencyExchangeRateList(currencyExchangeTempList)

        const currencyCodeToNameResponse = await fetch("https://openexchangerates.org/api/currencies.json");
        const currencyCodeToNameData = await currencyCodeToNameResponse.json();
        let currencyCodeToNameTempList = {}
        let currencyNameToCodeTempList = {}
        Object.keys(currencyCodeToNameData).forEach(currencyCode =>{
            currencyCodeToNameTempList[currencyCode] = currencyCodeToNameData[currencyCode]
            currencyNameToCodeTempList[currencyCodeToNameData[currencyCode]] = currencyCode
        })
        setCurrencyCodeToCurrencyNameList(currencyCodeToNameTempList)
        setCurrencyNameToCurrencyCodeList(currencyNameToCodeTempList)
        console.log(Constants.currencyCodeToNameList)  //DOESN'T WORK HERE
        console.log(Constants.currencyNameToCodeList)  //BOTH SHOWN AS 'undefinded'
    }

    //This function is used to calculate results whenever either 'currencyAmount' or 'currentBase' changed
    const doCalculations = () => {
        let currentAmount = currencyAmountRef.current.value
        let currentBase = currencyNameToCurrencyCodeList[currentBaseRef.current.value]
        let tempResultList = {}

        Object.keys(currencyExchangeRateList).forEach(currencyCode => {
            if(currencyCode !== currentBase){
                tempResultList[currencyCode] = (currentAmount / currencyExchangeRateList[currentBase]) * currencyExchangeRateList[currencyCode]
            }
        })
        setResultList(tempResultList)
        currentBaseRef.current.blur()
    }

    //The way i make function launch only once
    React.useEffect(() => {
        createDataArrays();
      }, []);

    
    return (
        <div className = "container">
            <div className = "header">
                <h1>Currency converter</h1>
            </div>
            <hr/>
            <div className = "dropboxContainer">
                <select className = "dropbox" onChange={doCalculations} size="1" name="currencies" ref = {currentBaseRef}>
                    {Object.keys(currencyExchangeRateList).map(currencyCode => {
                            return <option className = "dropboxElement" key={currencyCode}>{currencyCodeToCurrencyNameList[currencyCode]}</option>
                        })
                    }
                </select>
            </div>
            <div className = "inputfieldContainer">
                <input className = "inputfield" placeholder = "Input currency amount" maxLength = "9" onChange={doCalculations} ref = {currencyAmountRef}></input>
            </div>
            <div className = "resultsContainer">
                    <table className = "resultsTABLE" name="results" cellSpacing = "0">
                        <tbody className = "resultsBODY">
                            {Object.keys(resultsList).map(currencyCode => {
                                    return(
                                        <tr className="resultsTR" key = {currencyCode}>
                                            <td className = "resultsLEFT">{currencyCode}</td>
                                            <td className = "resultsRIGHT">{resultsList[currencyCode].toFixed(4)}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
            </div>
        </div>
    )
}
